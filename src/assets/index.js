import Logo from './logo/logo.png'
import LoginBg from './background/login.jpg'
import RegisterBg from './background/register.jpg'
import GoogleIcon from './icon/google-icon.svg'
import Loading from './icon/loading.gif'

export { Logo, LoginBg, RegisterBg, GoogleIcon, Loading }