import React from 'react'

const Quotes = (props) => {
    return (
        <>
            <div className={props.class + " col-sm-7 quote-log container "}>
                <div className="d-flex" style={{ height: 100 + '%' }}>

                    <blockquote className="blockquote text-center m-auto">
                        <p className="quotes">
                            <i className="iconify " data-icon="bx:bxs-quote-left" data-inline="true"></i>
                Everybody is a genius. But if you judge a fish by its ability to climb a tree, it will live its whole life believing that it is stupid.
          </p>
                        <footer className="blockquote-footer mt-3">Albert Einstein <cite title="Source Title">genius</cite></footer>

                    </blockquote>
                </div>
            </div>

        </>
    )
}

export default Quotes
