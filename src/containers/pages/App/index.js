import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Register, Login, Notfound } from '../';
import Dashboard from '../Dashboard';
import { Provider } from 'react-redux';
import { store } from "../../../config/redux/store"

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route path="/" component={Dashboard} exact />
          <Route path="/login" component={Login} exact />
          <Route path="/register" component={Register} exact />
          <Route path="" component={Notfound} />
        </Switch>
      </Router>
    </Provider >
  );

}

export default App;
