import React from "react";
import { Link } from "react-router-dom";
import "./style.css";
import { FloatInput, GButton, Wrapper } from '../../../components/atoms';
import { connect } from 'react-redux'
import { fireDispatch } from '../../../config/redux/action/firebase';
import { Quotes } from '../../organism';


class Register extends React.Component {

  render() {

    const data = ({
      email: this.props.userName,
      password: this.props.passName
    })

    const handleSubmit = () => {
      this.props.register(data)
    }



    return (
      <>

        <div className="row">

          <Quotes class='reg-wrapper' />
          <div className="col-sm-5 d-flex reg-container">
            <div className="container m-auto">
              <Wrapper loading={this.props.loading} />
              <h2 className="mb-5 auth-title">Hoonian.com</h2>
              <div className="container m-auto">
                <FloatInput placeholder='Username' type='text' icon='bx:bxs-user-circle' id='username' />
                <div className='my-3' />
                <FloatInput placeholder='Password' type='password' icon='gridicons:lock' id='password' />
                <button
                  className="btn btn-primary aqua mt-4 text-center submit"
                  onClick={handleSubmit}
                >
                  SignUp {this.props.error}
                </button>

              </div>
              <Link
                to="/login"
                className="link d-flex justify-content-center mt-3"
              >
                Already have an account? SignIn Now!
          </Link>
              <GButton />

            </div>
          </div>

        </div >
      </>
    )

  }

}
// const error = new Error(err)

const reduxState = (state) => ({
  passName: state.password,
  userName: state.username,
  loading: state.loading
})

const reduxDispatch = (dispatch) => ({
  register: (e) => dispatch(fireDispatch(e))
})

export default connect(reduxState, reduxDispatch)(Register);
