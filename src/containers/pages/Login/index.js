import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./style.css";
import { connect } from 'react-redux';
import { FloatInput, GButton, Wrapper } from '../../../components/atoms';
import { fireDispatchLogin } from '../../../config/redux/action/firebase'
import { Quotes } from '../../organism';



class Login extends Component {
  render() {
    const data = ({
      email: this.props.userName,
      password: this.props.passName
    })

    const setUser = () => {
      this.props.login(data)
    }
    return (
      <>
        <div className="row">
          <div className="col-sm-5 d-flex reg-container">
            <div className="container m-auto">
              <Wrapper loading={this.props.loading} />

              <h2 className="mb-5 auth-title">Hoonian.com </h2>
              <div className="container m-auto">
                <div className="alert alert-danger" role="alert">
                  {this.props.error}
                </div>

                <FloatInput placeholder='Username' type='text' icon='bx:bxs-user-circle' id='username' />
                <div className='my-3' />
                <FloatInput placeholder='Password' type='password' icon='gridicons:lock' id='password' />
                <button
                  className="btn btn-primary aqua mt-4 text-center submit"
                  onClick={setUser}
                >
                  Login
                </button>

              </div>
              <Link
                to="/register"
                className="link d-flex justify-content-center mt-3"
              >
                Not Registered? Create an Account
          </Link>
              <GButton />

            </div>
          </div>
          <Quotes class='login-wrapper' />
        </div >
      </>
    )
  }
};

const reduxState = (state) => ({
  passName: state.password,
  userName: state.username,
  loading: state.loading,
  error: state.error
})

const reduxDispatch = (dispatch) => ({
  login: (e) => dispatch(fireDispatchLogin(e))
})

export default connect(reduxState, reduxDispatch)(Login);
