import React from 'react';
import "./style.css"

function notFound() {
    return (
        <div className="nf-wrapper">
            <h3 className="err-nf">404</h3>
            <h5 className="err-nf-p">Your Page Not Found</h5>
        </div>
    );
}

export default notFound;   