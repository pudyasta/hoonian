import Login from './Login'
import Register from './Register'
import Notfound from './Notfound'

export { Login, Register, Notfound }