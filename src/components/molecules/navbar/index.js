import React from "react";
import "./style.css";
import { Logo } from "../../../assets";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light bg-light ">
        <div className="container">
          <Link className="navbar-brand" to="/">
            <img src={Logo} alt=""></img>
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarTogglerDemo03"
            aria-controls="navbarTogglerDemo03"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul className="navbar-nav m-auto mb-2 mb-lg-0 text-center">
              <li className="nav-item">
                <Link className="nav-link" aria-current="page" to="/interior">
                  House Guide
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" aria-current="page" to="/interior">
                  Inspirasi
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" aria-current="page" to="/interior">
                  Interior
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" aria-current="page" to="/interior">
                  Berita
                </Link>
              </li>
            </ul>
            <form className="d-flex justify-content-center">
              <input
                className="form-control"
                type="search"
                placeholder="Search"
                aria-label="Search"
              ></input>
              <button className="btn btn-primary aqua" type="submit">
                Cari
              </button>
            </form>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Navbar;
