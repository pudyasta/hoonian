import React from 'react';
import './style.css';
import { Loading } from '../../../assets'

const Wrapper = (props) => {
    if (props.loading === true) {
        return (
            <div className="wrapper d-flex">
                <img src={Loading} className='m-auto' width='50px' alt='loading-icon' />
            </div>
        )
    } else {

        return (
            <div></div>
        )
    }
}

export default Wrapper
