import React from 'react';
import './style.css';
import { GoogleIcon } from '../../../assets/';
import firebase from '../../../config/firebase';

const gButton = () => {
    const loginGoogle = () => {
        var provider = new firebase.auth.GoogleAuthProvider();
        provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
        firebase.auth().signInWithPopup(provider).then(function (result) {
            // // This gives you a Google Access Token. You can use it to access the Google API.
            // var token = result.credential.accessToken;
            // // The signed-in user info.
            // var user = result.user;
            // // ...
        }).catch(function (error) {
            // // Handle Errors here.
            // var errorCode = error.code;
            // var errorMessage = error.message;
            // // The email of the user's account used.
            // var email = error.email;
            // // The firebase.auth.AuthCredential type that was used.
            // var credential = error.credential;
            // // ...
        });
    }
    return (
        <div className='container d-flex'>
            <button
                className="btn btn-light mt-4 text-center google-button shadow m-auto"
                onClick={loginGoogle}
            >
                <img src={GoogleIcon} width='25px' className="mx-3" alt="google-icon" />
                Login with Google
                </button>
        </div>
    )
}

export default gButton
