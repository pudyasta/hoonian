import React from 'react'
import './style.css'
import { connect } from 'react-redux'
import { userDispatch } from '../../../config/redux/action/user'


const FloatInput = (props) => {
    const setVal = (e) => {
        return props.changeVal(e);
    }
    return (
        <>

            <span className="iconify reg-icon" data-icon={props.icon} data-inline="false"></span>
            <input type={props.type} className="form-control" placeholder={props.placeholder} id={props.id} onChange={setVal} />
        </>
    )
}



const reduxState = (state) => ({
    userName: state.username,
    passName: state.password,
})

const reduxDispatch = (dispatch) => ({
    changeVal: (e) => dispatch(userDispatch(e))
})
export default connect(reduxState, reduxDispatch)(FloatInput)
