export const userDispatch = (e) => (dispatch) => {
    if (e.target.id === 'username') {
        return dispatch({ type: 'CHANGE_USER', value: e.target.value })
    }
    return dispatch({ type: 'CHANGE_PASS', value: e.target.value })
}