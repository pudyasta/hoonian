import firebase from '../../../firebase'

export const fireDispatch = (data) => (dispatch) => {
    dispatch({ type: 'CHANGE_LOADING', value: true })
    firebase.auth().createUserWithEmailAndPassword(data.email, data.password)
        .then(res => dispatch({ type: 'CHANGE_LOADING', value: false }))
        .catch(err => {
            dispatch({ type: 'CHANGE_LOADING', value: false })
        })
}

export const fireDispatchLogin = (data) => (dispatch) => {
    dispatch({ type: 'CHANGE_LOADING', value: true })
    firebase.auth().signInWithEmailAndPassword(data.email, data.password)
        .then(res => {
            console.log(res)
            dispatch({ type: 'CHANGE_LOADING', value: false });
        })
        .catch(err => {
            console.log(err.message)
            dispatch({ type: 'CHANGE_ERROR', value: '' });
            dispatch({ type: 'CHANGE_LOADING', value: false });
            dispatch({ type: 'CHANGE_ERROR', value: err.message });
        })
}
