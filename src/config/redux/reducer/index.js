const initialState = {
    login: "true",
    username: 'ssss',
    password: '',
    loading: false,
    error: []
}

const reducer = (state = initialState, action) => {
    if (action.type === 'CHANGE_USER') {
        return {
            ...state,
            username: action.value
        }
    }
    if (action.type === 'CHANGE_PASS') {
        return {
            ...state,
            password: action.value
        }
    }
    if (action.type === 'CHANGE_LOADING') {
        return {
            ...state,
            loading: action.value
        }
    }
    if (action.type === 'CHANGE_ERROR') {
        return {
            ...state,
            error: action.value
        }
    }
    return state
}
export default reducer

